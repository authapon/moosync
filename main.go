package main

import (
	"fmt"
	"os"
	"os/exec"
	"sync"
)

var (
	mu = &sync.Mutex{}
)

func unison(f1, f2 string) {
	mu.Lock()
	defer mu.Unlock()

	cmd := exec.Command("unison", f1, f2, "-batch=true", "-auto=true", "-confirmbigdel=false", "-silent=true")
	cmd.Run()
}

func inotifywait(f string) {
	cmd := exec.Command("inotifywait", "-r", "-e", "modify,attrib,move,create,delete", f)
	cmd.Run()
}

func unixSleep(s string) {
	cmd := exec.Command("sleep", s)
	cmd.Run()
}

func watch(f1, f2 string) {
	for {
		fmt.Printf("watching...\n")
		inotifywait(f1)
		fmt.Printf("Syncing...\n")
		unison(f1, f2)
	}
}

func looper(f1, f2, f3 string) {
	for {
		fmt.Printf("Loop Syncing...\n")
		unison(f1, f2)
		unixSleep(f3)
	}
}

func main() {
	if len(os.Args) != 4 {
		fmt.Printf("Usage: moosync /home/user/folder ssh://user@10.0.0.1//home/user/syncfolder 30s\n")
		os.Exit(1)
	}
	go watch(os.Args[1], os.Args[2])
	go looper(os.Args[1], os.Args[2], os.Args[3])
	select {}
}
