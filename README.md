# moosync
moosync is a tool for synchronized local folder to remote machine

*This project is made for personal used. You can take it by your own risk.*

**Dependency:**
* unison
* inotifywait

**Usage:**
* moosync /home/user/folder ssh://user@10.0.0.1//home/user/syncfolder 30s
